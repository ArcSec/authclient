<?php

class AuthClient
{
    private $settings;
    private $cookie_file_path;

    public $cookies;
	
    public $default_curl_options = array
    (
        CURLOPT_HEADER => false,
        CURLOPT_HTTPHEADER => array
        (
            "Accept: */*",
            "Connection: Keep-Alive",
        ),
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0",
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FOLLOWLOCATION => 1,
    );
    
    public function __construct()
    {
        // Get our login credentials
        require(__DIR__.'/settings.php');
        $this->settings = $_SETTINGS;
        
        $this->Reset();
    }
    
    private function Reset()
    {
        $this->cookies = array();
        $this->cookie_file_path = __DIR__."/cookies.txt";
        exec('echo "" > cookies.txt');

        $this->default_curl_options[CURLOPT_COOKIEFILE] = $this->cookie_file_path;
        $this->default_curl_options[CURLOPT_COOKIEJAR] = $this->cookie_file_path;
    }
    
    public function GetPage($options = array())
    {
        // begin script
        $ch = curl_init();
		
        curl_setopt_array($ch, $options);

        return curl_exec($ch);
    }
    
    public function LoginRSI()
    {
        // Clear out temp cookies
        $this->Reset();
		
        // Temp URL to get required cookies
        $curl_opts = $this->default_curl_options;
        $curl_opts[CURLOPT_URL] = "https://robertsspaceindustries.com/connect";
        $curl_opts[CURLOPT_HTTPHEADER][] = "Referer: https://robertsspaceindustries.com/";
		
        $content = $this->GetPage($curl_opts);
        
		
        // Retrieve cookies
        $string = file_get_contents($this->cookie_file_path);
        $cookies = array();
        preg_match_all('|\.robertsspaceindustries\.com(\s+\S+){4}\s+(?<name>\S+)\s+(?<value>\S+)\s*|', $string, $cookies, PREG_SET_ORDER);
		
		
        // Switch to login URL
        $curl_opts = $this->default_curl_options;
        $curl_opts[CURLOPT_URL] = "https://robertsspaceindustries.com/api/account/signin";
        $curl_opts[CURLOPT_HTTPHEADER][] = "X-Requested-With: XMLHttpRequest";
        $curl_opts[CURLOPT_HTTPHEADER][] = "Referer: https://robertsspaceindustries.com/connect";
		
		// Set new cookies
        foreach($cookies as $cookie)
        {
            if($cookie['name'] == 'Rsi-Token')
            $curl_opts[CURLOPT_HTTPHEADER][] = "X-Rsi-Token: ".$cookie['value'];
            
            $this->cookies[$cookie['name']] = $cookie['value'];
        }
        
        // set postfields for login credentials
        $fields['username'] = $this->settings['clients']['rsi']['username'];
        $fields['password'] = $this->settings['clients']['rsi']['password'];
        $fields['remember'] = 1;
        $curl_opts[CURLOPT_POSTFIELDS] = http_build_query($fields);
        $curl_opts[CURLOPT_POST] = true;

        // perform login
        $result = $this->GetPage($curl_opts);
        
        return $result;
    }


    private function GetFormFields($data)
    {
        if (preg_match('/(<form class="signin-form".*?<\/form>)/is', $data, $matches)) 
        {
            $inputs = $this->GetInputs($matches[1]);

            return $inputs;
        }
        else
        {
            die('Didn\'t find login form');
        }
    }

    private function GetInputs($form)
    {
        $inputs = array();

        $elements = preg_match_all('/(<input[^>]+>)/is', $form, $matches);

        if ($elements > 0) 
        {
            for($i = 0; $i < $elements; $i++) 
            {
                $el = preg_replace('/\s{2,}/', ' ', $matches[1][$i]);

                if (preg_match('/name=(?:["\'])?([^"\'\s]*)/i', $el, $name)) 
                {
                    $name  = $name[1];
                    $value = '';

                    if (preg_match('/value=(?:["\'])?([^"\'\s]*)/i', $el, $value)) 
                    {
                        $value = $value[1];
                    }

                    $inputs[$name] = $value;
                }
            }
        }

        return $inputs;
    }
}