<?php

/*
 * 
 * COPY AND RENAME THIS FILE TO settings.php TO USE IT
 * 
 */

$_SETTINGS = array(
    
    // Proxy client settings
    'clients' => array(
        'rsi' => array(
            'username' => '',    // Handle of the RSI account you wish to use
            'password' => '',    // Lowercase MD5 of the password of the RSI account you wish to use
        ),
    ),
	
);